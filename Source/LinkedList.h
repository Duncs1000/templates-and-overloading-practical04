//
//  LinkedList.h
//  CommandLineTool
//
//  Created by Duncan Whale on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef LINKED_LIST
#define LINKED_LIST

/**
 Template class to store and manipulate a singly-linked list of objects.
 
 This can be a linked-list of primitives or any other non-polymorphic object with copy constructor and assignment operators. e.g.
 
 @code
 struct MyObject
 {
 int x, y, z;
 };
 
 SinglyLinkedList<MyObject> myList;
 MyObject object0 = {0, 0, 0};
 MyObject object1 = {1, 1, 1};
 
 myList.add (object0);
 myList.add (object1);
 
 int numItems = myList.size(); // returns 2
 MyObject object2copy = myList.getLast();
 @endcode
 
 */

template <class Type>
class LinkedList
{
public:
    /**
     Constructor. Creates an empty list.
     */
    LinkedList()
    {
        head = nullptr;
    }
    
    /**
     Destructor. Clears up the contents of the list.
     */
    ~LinkedList()
    {
        deleteAll();
    }
    
    /**
     Returns the number of items in the list.
     @returns   the size of the list
     */
    int size() const
    {
        int total = 0;
        
        for (Node* current = head; current != nullptr; current = current->next)
            ++total;
        
        return total;
    }
    
    /**
     Adds an item to the end of the list/
     @param newValue value to be added to the end of the array.
     */
    void add (Type newValue)
    {
        Node* newNode = new Node;
        
        newNode->value = newValue;
        newNode->next = nullptr;
        
        if (isEmpty())
            head = newNode;
        else
            getLastNode()->next = newNode;
    }
    
    /**
     Removes an item from the array.
     @param index the index of the element to remove.
     */
    void remove (int index)
    {
        Node* current = head;   // First will have the node after deletion.
        
        if (current == nullptr)
            return;
        
        Node* previous = nullptr;
        while (current != nullptr)
        {
            if (index-- == 0)
            {
                if (previous == nullptr)
                    head = current->next;
                else
                    previous->next = current->next;
                
                delete current;
                break;
            }
            else
            {
                previous = current;
                current = current->next;
            }
        }
    }
    
    /**
     Returns the item at a given index in the list, or the end if index is out of range. Don't call this if the list is empty.
     @param index   the index of the element to be returned
     @returns       the value of the item at @index
     */
    
    Type get (int index) const
    {
        Node* temp = head;
        
        while (--index >= 0 && temp->next != nullptr)
            temp = temp->next;
        
        return temp->value;
    }
    
    /**
     Returns the item at a given index in the list. If @index is out of range it returns the last item in the list.
     */
    Type& operator[] (int index) const
    {
        Node* temp = head;
        
        while (--index >= 0 && temp->next != nullptr)
            temp = temp->next;
        
        return temp->value;
    }
    
    /**
     Iterates the list, calling the delete operator on all of its elements, leaving the list empty.
     */
    void deleteAll()
    {
        while (head != nullptr)
        {
            Node* temp = head;
            head = head->next;
            delete temp;
        }
    }
    
    /**
     Returns true if the list is empty.
     @returns   true if empty
     */
    bool isEmpty() const
    {
        return head == nullptr;
    }
    
    /**
     Reverses the list using an iterative method.
     */
    void reverse()
    {
        if (isEmpty())
            return;
        
        Node* previous = nullptr;
        Node* next;
        
        do
        {
            next = head->next;
            head->next = previous;
            previous = head;
            head = next;
        } while (next != nullptr);
        head = previous;
    }
   
    bool operator==(const LinkedList& otherList)
    {
        if (size() == otherList.size())
        {
            int result = 0;
            
            for (int i = 0; i < size(); i++)
            {
                if (get(i) != otherList.get(i))
                    result += 1;
            }
            
            if (result == 0)
                return true;
        }
        
        return false;
    }
    
    bool operator!=(const LinkedList& otherList)
    {
        if (size() == otherList.size())
        {
            int result = 0;
            
            for (int i = 0; i < size(); i++)
            {
                if (get(i) != otherList.get(i))
                    result += 1;
            }
            
            if (result == 0)
                return false;
        }
        
        return true;
    }
    
private:
    struct Node
    {
        Type value;
        Node* next;
    };
    
    // Always check the list is not empty before calling this.
    Node* getLastNode()
    {
        Node* temp = head;
        while (temp->next != nullptr)
            temp = temp->next;
        
        return temp;
    }
    
    Node* head;
};

#include <iostream>
/**
 Function that tests this class to make sure everything works.
 @returns   True if successful.
 */
bool testList()
{
    LinkedList<float> list;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (list.size() != 0)
    {
        std::cout << "Size is incorrect.\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        list.add (testArray[i]);
        
        if (list.size() != i + 1)
        {
            std::cout << "After adding item '" << i << "', size is incorrect.\n";
            return false;
        }
        
        if (list.get (i) != testArray[i])
        {
            std::cout << "Value at index " << i << " recalled incorrectly.\n";
            return false;
        }
    }
    
    // Reverse list.
    list.reverse();
    for (int i = 0; i < testArraySize; i++)
    {
        if (list.get(i) != testArray[testArraySize - 1 - i])
        {
            std::cout << "Reverse did not work.\n";
            return false;
        }
    }
    
    // Put back forward.
    list.reverse();
    
    // Removing first.
    list.remove(0);
    if (list.size() != testArraySize - 1)
    {
        std::cout << "Incorrent size after removing item.\n";
        return false;
    }
    
    for (int i = 0; i < list.size(); i++)
    {
        if (list.get(i) != testArray[ i+ 1])
        {
            std::cout << "Problems removing items.\n";
            return false;
        }
    }
    
    // Removing last.
    list.remove (list.size() - 1);
    if (list.size() != testArraySize - 2)
    {
        std::cout << "Incorrent size after removing item.\n";
        return false;
    }
    
    for (int i = 0; i < list.size(); i++)
    {
        if (list.get(i) != testArray[i + 1])
        {
            std::cout << "Problems removing items.\n";
            return false;
        }
    }
    
    // Remove second item.
    list.remove (1);
    if (list.size() != testArraySize - 3)
    {
        std::cout << "Incorrect size after removing item.\n";
        return false;
    }
    
    if (list.get (0) != testArray[1])
    {
        std::cout << "Problems removing items.\n";
        return false;
    }
    
    if (list.get (1) != testArray[3])
    {
        std::cout << "Problems removing items.\n";
        return false;
    }
    
    if (list.get (2) != testArray[4])
    {
        std::cout << "Problems removing items.\n";
        return false;
    }
    
    for (int i = 0; i < list.size(); i++)
    {
        std::cout << "Value at index " << i << " = " << list.get(i) << ".\n";
    }
    
    std::cout << "All linked list tests passed!\n";
    return true;
}

#endif /* Defined LinkedList */