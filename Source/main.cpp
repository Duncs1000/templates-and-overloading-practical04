//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"
#include "Utility.h"
#include "Number.h"
#include "LinkedList.h"


//------FUNCTION DECLARATIONS------//


//------MAIN------//

int main (int argc, const char* argv[])
{
    LinkedList<double> opTestList, opTestListTwo;
    
    opTestList.add(3.15);
    opTestList.add(7.12);
    opTestList.add(9.13);
    
    opTestListTwo.add(3.15);
    opTestListTwo.add(7.12);
    opTestListTwo.add(9.13);
    
    for (int i = 0; i < opTestList.size(); i++)
        std::cout << "The value at element " << i << " is " << opTestList[i] << ".\n";
    
    return 0;
}


//------FUNCTION DEFINITIONS------//