//
//  Number.h
//  CommandLineTool
//
//  Created by Duncan Whale on 10/16/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef NUMBER
#define NUMBER

template <class Type>
class Number
{
public:
    Type get() const
    {
        return value;
    }
    
    void set(Type newValue)
    {
        value = newValue;
    }
    
private:
    Type value;
};

#endif  // Defined 'Number'.