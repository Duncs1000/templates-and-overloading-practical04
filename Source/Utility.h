//
//  Utility.h
//  CommandLineTool
//
//  Created by Duncan Whale on 10/16/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef UTILITY
#define UTILITY


//------TEMPLATE FUNCTIONS------//

// Returns the highest value.
template <typename Type>
Type max (const Type a, const Type b)
{
    if (a < b)
        return b;
    else
        return a;
}

// Returns the lowest value.
template <typename Type>
Type min (const Type a, const Type b)
{
    if (a < b)
        return a;
    else
        return b;
}

// Returns the sum of the arguments.
template <typename Type>
Type add (const Type a, const Type b)
{
    return a + b;
}

// Prints 'size' amount of array elements.
template <typename Type>
void print (const Type array[], const int size)
{
    for (int i = 0; i < size; i++)
        std::cout << "Array element " << i + 1 << " is " << array[i] << ".\n";
}

// Returns the average of an array.
template <typename Type>
double getAverage(const Type array[], const int size)
{
    double result = 0;
    
    for (int i = 0; i < size; i++)
        result += array[i];
    
    return result / size;
}


//------FUNCTION TESTS------//

// Tests the 'min' function.
void testMinFunc()
{
    double minTestA, minTestB;
    
    std::cout << "Minimum Test: Please enter two numbers.\n";
    std::cin >> minTestA;
    std::cin >> minTestB;
    std::cout << "The minimum of your numbers is " << min(minTestA, minTestB) << ".\n";
}

// Tests the 'add' function.
void testAddFunc()
{
    double addTestA, addTestB;
    
    std::cout << "Addition Test: Please enter two numbers.\n";
    std::cin >> addTestA;
    std::cin >> addTestB;
    std::cout << "The sum of your numbers is " << add(addTestA, addTestB) << ".\n";
}

// Tests the 'print' function.
void testPrintFunc()
{
    const int arrayPrintSize = 5;
    double arrayPrintTest[arrayPrintSize];
    
    std::cout << "Array Print Test:\n";
    
    for (int i = 0; i < arrayPrintSize; i++)
    {
        std::cout << "Please enter element " << i + 1 << " of the array.\n";
        std::cin >> arrayPrintTest[i];
    }
    
    print(arrayPrintTest, arrayPrintSize);
}

// Tests the 'getAverage' function.
void testGetAverageFunc()
{
    const int arrayAvgSize = 5;
    double arrayAvgTest[arrayAvgSize];
    
    std::cout << "Array Average Test.\n";
    
    for (int i = 0; i < arrayAvgSize; i++)
    {
        std::cout << "Please enter element " << i + 1 << " of the array.\n";
        std::cin >> arrayAvgTest[i];
    }
    
    std::cout << "The average of your array is " << getAverage(arrayAvgTest, arrayAvgSize) << ".\n";
}

#endif  // Defined 'Utility'.