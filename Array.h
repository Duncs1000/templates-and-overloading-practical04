//
//  Array.h
//  CommandLineTool
//
//  Created by Tom Mitchell on 07/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//
#include <iostream>

#ifndef H_ARRAY
#define H_ARRAY

/**
 Class that manages a dynamic array of floats
 */
template <class Type>
class Array
{
public:
    /** Constructor */
    Array()
    {
        arraySize = 0;
        data = nullptr;
    }
    
    /** Destructor */
    ~Array()
    {
        if (data != nullptr)
        {
            delete [] data;
        }
    }
    
    /** 
     Returns the size of the array - the number of elements stored in the array 
     @returns       the size of the array
     */
    int size() const
    {
        return arraySize;
    }
    
    /** 
     adds the @value argument as a new item at the end of the array 
     @param newValue    value to be added to the end of the array
     */
    void add (Type value)
    {
        Type* tempPtr = new Type[size() + 1];
        
        for (int i = 0; i < size(); i++)
            tempPtr[i] = data[i];
        
        tempPtr[size()] = value;
        
        if (data != nullptr)
            delete [] data;
        
        data = tempPtr;
        arraySize++;
    }
    
    /** 
     returns the item in the array at @index
     @param index    the item index in the array that should be returned
     @returns       the value of the item at @index
     */
    Type get (int index) const
    {
        if (index >= 0 && index < size())
            return data[index];
        else
            return 0.f;
    }
    
    /** 
     removes the item at @index
     @param index    the item index in the array that should be removed
     */
    void remove (int index)
    {
        if (index < 0 || index >= size())
            return;
        
        Type* tempPtr = new Type[size() - 1];
        
        for (int i = 0; i < size(); i++)
        {
            if (i < index)
                tempPtr[i] = data[i];
            else if (i > index)
                tempPtr[i - 1] = data[i];
        }
        
        delete [] data;
        data = tempPtr;
        arraySize--;
    }
    
    /** 
     Reverses the array using an iterative method 
     */
    void reverse()
    {
        int halfArraySize = size() / 2; //odd numbers leave the middle element where it is
        for (int i = 0; i < halfArraySize; i++)
        {
            Type temp = data[i];
            data[i] = data[size() - 1 - i];
            data[size() - 1 - i] = temp;
        }
    }
    
    
    //------OPERATOR OVERLOADING------//
    
    bool operator==(const Array& otherArray)
    {
        if (size() == otherArray.size())
        {
            int result = 0;
            
            for (int i = 0; i < size(); i++)
            {
                if (get(i) != otherArray.get(i))
                    result += 1;
            }
            
            if (result == 0)
                return true;
        }
        
        return false;
    }
    
    bool operator!=(const Array& otherArray)
    {
        if (size() == otherArray.size())
        {
            int result = 0;
            
            for (int i = 0; i < size(); i++)
            {
                if (get(i) != otherArray.get(i))
                    result += 1;
            }
            
            if (result == 0)
                return false;
        }
        
        return true;
    }
    
    Type& operator[] (int index) const
    {
        if (index >= 0 && index < size())
            return data[index];
        else
        {
            std::cout << "Error. Out of array range. Defaulted to last element.\n";
            return data[arraySize - 1];
        }
    }
    
private:
    int arraySize;
    Type* data;
};

/**
 Function that tests this class to make sure everything works.
 @returns   True if successful
 */

#include <iostream>
static bool testArray()
{
    Array<double> array;
    const double testArray[] = {0, 1, 2, 3, 4, 5};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    array.reverse();
    for (int i = 0; i < testArraySize; i++)
    {
        if (array.get(i) != testArray[testArraySize - 1 - i])
        {
            std::cout << "reverse did not work\n";
            return false;
        }
    }
    //put back forward
    array.reverse();
    
    //removing first
    array.remove (0);
    if (array.size() != testArraySize - 1)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    for (int i = 0; i < array.size(); i++)
    {
        if (array.get(i) != testArray[i+1])
        {
            std::cout << "problems removing items\n";
            return false;
        }
    }
    
    //removing last
    array.remove (array.size() - 1);
    if (array.size() != testArraySize - 2)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    for (int i = 0; i < array.size(); i++)
    {
        if (array.get(i) != testArray[i + 1])
        {
            std::cout << "problems removing items\n";
            return false;
        }
    }
    
    //remove second item
    array.remove (1);
    if (array.size() != testArraySize - 3)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    if (array.get (0) != testArray[1])
    {
        std::cout << "problems removing items\n";
        return false;
    }
    
    if (array.get (1) != testArray[3])
    {
        std::cout << "problems removing items\n";
        return false;
    }
    
    if (array.get (2) != testArray[4])
    {
        std::cout << "problems removing items\n";
        return false;
    }
    
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        std::cout << "value at index "<< i << " = " << array.get (i) << "\n";
    //    }
    
    std::cout << "all Array tests passed\n";
    return true;
}

#endif /* H_ARRAY */